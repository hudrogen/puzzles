import org.apache.log4j.xml.DOMConfigurator;
import utils.DataBaseManager;
import utils.SMTPsender;

/**
 * Created by hudrogen on 14.04.17.
 */
public class Main {

    static {
        DOMConfigurator.configure("log4j.xml");
    }

    public static void main(String[] args) {
        DataBaseManager dbm = new DataBaseManager();
        dbm.init();
        //dbm.getAllFRomDB();

        //SMTPsender emailsender = new SMTPsender();
        //emailsender.sendMessage("Тестовое сообщение!");
    }
}
