package models;

/**
 * Класс описывающий ответ на загадку
 */

public class PuzzleAnswer {
    private int id_answer;  //уникальный id ответа
    //private Puzzle puzzle;  //ссылка на саму задачу
    private int puzzle_id;
    private String body; //текст ответа
}
