package utils;

import org.apache.log4j.Logger;

import java.sql.*;

public class DataBaseManager {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    PreparedStatement pstm = null;

    private static final Logger log = Logger.getLogger(DataBaseManager.class);

    public DataBaseManager() {
    }

    public void init(){
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/puzzles", "root", "1");
            stmt = conn.createStatement();
            //log.debug("Connected to MySQL!");


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }



    public void getAllFRomDB() {
        init();
        String sqlQuery = "select * from users";
        try {
                rs = stmt.executeQuery(sqlQuery);
                while (rs.next()){
                String name = rs.getString("name");
                String email = rs.getString("email");
                System.out.println(name + " " + email);

             }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
