package utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


public class SMTPsender {

    public void sendMessage(String messageStr) {

        //вынести в файл конфигурации
        final String username = "a.salimgaraev.stc@innopolis.ru";
        final String password = "inser pass here";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.trust", "mail.innopolis.ru");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "mail.innopolis.ru");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("a.salimgaraev.stc@innopolis.ru"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("hudrogen2@yandex.ru"));
            message.setSubject("Testing Subject");
            message.setText(messageStr);


            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }
}