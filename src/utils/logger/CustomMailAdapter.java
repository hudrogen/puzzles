package utils.logger;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;
import utils.SMTPsender;

import java.util.ArrayList;

/**
 * Created by hudrogen on 14.04.17.
 * Отвечает на вопрос куда слать логи
 * В данном случае логи загоняем в коллекцию
 */
public class CustomMailAdapter extends AppenderSkeleton{

    //ArrayList<LoggingEvent> eventsList = new ArrayList();
    SMTPsender logSender = new SMTPsender();

    @Override
    protected void append(LoggingEvent event) {
        switch(event.getLevel().toInt()){
            case Level.INFO_INT:
                //your decision
                break;
            case Level.DEBUG_INT:
                //your decision
                logSender.sendMessage("DEBUG: " + (String) event.getMessage());
                break;
            case Level.ERROR_INT:
                //System.out.println("its an error " + event.getMessage());
                //logSender.sendMessage("ERROR: " + (String) event.getMessage());
                break;
            case Level.WARN_INT:
                //System.out.println("its a warn ***" + event.getMessage());
                //logSender.sendMessage("WARN: " + (String) event.getMessage());
                break;
            case Level.TRACE_INT:
                //your decision
                break;
            default:
                //your decision
                break;
        }
    }


    public void close() {
    }

    public boolean requiresLayout() {
        return false;
    }
}